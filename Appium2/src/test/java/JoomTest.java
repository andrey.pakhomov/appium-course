import config.AndroidSettingsConfig;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import lombok.SneakyThrows;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import java.net.URL;

public class JoomTest {
    /**
     * Экземпляр интерфейса с основными настройками драйвера
     */
    private final static AndroidSettingsConfig androidConfig = ConfigFactory.create(AndroidSettingsConfig.class, System.getenv());

    /**
     * Экземпляр андроид драйвера
     */
    private AndroidDriver driver;
    @Test
    @SneakyThrows
    public void test(){
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,androidConfig.deviceName());
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,androidConfig.platformName());
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION,androidConfig.platformVersion());
        capabilities.setCapability(MobileCapabilityType.UDID,androidConfig.udid());
        capabilities.setCapability(MobileCapabilityType.APP,androidConfig.app());
        capabilities.setCapability("appWaitActivity",androidConfig.appWaitActivity());

        driver = new AndroidDriver(new URL(androidConfig.url()), capabilities);
    }

}
