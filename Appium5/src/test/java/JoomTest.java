import lombok.SneakyThrows;
import org.testng.annotations.Test;
import page.JoomPage;
import page.SearchPage;
import page.window.PopUp;
import page.window.Stories;

public class JoomTest extends BaseTest {
    /**
     * Тест на выбор валюты
     */
    @SneakyThrows
    @Test
    public void SelectionCurrency() {
        //Проверка на всплывающие Stories и PopUp окна
        new Stories(getDriver())
                .clickStoriesCloseTransitionJoomPage();
        new PopUp(getDriver())
                .clickStoriesCloseTransitionJoomPage();
        //Основной тест
        new JoomPage(getDriver())
                .clickProfileButtonTransitionProfilePage()
                .clickSettingButtonTransitionSettingPage()
                .clickCurrencyChapterTransitionCurrencyView()
                .clickCurrencyRadioButtonDollarTransitionSettingPage()
                .clickNavigationBackButtonTransitionProfilePage()
                .clickNavigationHomeButtonTransitionJoomPage()
                .clickProductButtonTransitionProductPage()
                .assertCurrencyProductPage();
    }

    @SneakyThrows
    @Test
    public void PriceSorting() {
        //Проверка на всплывающие Stories и PopUp окна
        new Stories(getDriver())
                .clickStoriesCloseTransitionJoomPage();
        new PopUp(getDriver())
                .clickStoriesCloseTransitionJoomPage();
        //Основной тест
        new JoomPage(getDriver())
                .clickInputSearchTransitionSearchPage();
        new SearchPage(getDriver())
                .clickAndSendKeysForInputSearchTransitionSearchResultPage()
                .clickSortButtonTransitionSettingSortView()
                .clickAscendingPriceRadioButtonAndTransitionSearchResultPage()
                .getPrice();

    }

    @SneakyThrows
    @Test
    public void ActiveTabs() {
        //Проверка на всплывающие Stories и PopUp окна
        new Stories(getDriver())
                .clickStoriesCloseTransitionJoomPage();
        new PopUp(getDriver())
                .clickStoriesCloseTransitionJoomPage();
        //Основной тест
        new JoomPage(getDriver())
                .swipeLeftInJoomPage();
        new JoomPage((getDriver()))
                .isSelectedActiveTabs();
    }

    @SneakyThrows
    @Test
    public void AddToFavorites() {
        //Проверка на всплывающие Stories и PopUp окна
        new Stories(getDriver())
                .clickStoriesCloseTransitionJoomPage();
        new PopUp(getDriver())
                .clickStoriesCloseTransitionJoomPage();
        //Основной тест
        new JoomPage(getDriver())
                .clickProductButtonTransitionProductPage()
                .clickLikeButtonAndClickBackButtonTransitionJoomPage()
                .clickProfileButtonTransitionProfilePage()
                .clickFavoritesButtonTransitionFavoritesPage()
                .swipeDownInFavoritesPage()
                .countProductView();
    }
}