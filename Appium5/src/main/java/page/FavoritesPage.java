package page;

import com.google.common.collect.ImmutableMap;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

/**
 * Класс страницы Избранное
 */
public class FavoritesPage {
    /**
     * Переменная с текущим андроид драйвером
     */
    private final AndroidDriver driver;
    /**
     * Конструктор создания страницы
     * @param androidDriver экземпляр драйвера
     */
    public FavoritesPage(AndroidDriver androidDriver){
        driver = androidDriver;
        PageFactory.initElements(driver, this);
    }

    /**
     * Элемент Раздел Избранное
     */
    @FindBy(id = "com.joom:id/feed_recycler")
    private WebElement favoritesFeed;
    /**
     * Элемент Продукт
     */
    @FindBy(id = "com.joom:id/product_view")
    private WebElement productView;

    public FavoritesPage countProductView() {
        List<WebElement> list = driver.findElements(By.id("com.joom:id/product_view"));
        int resultCount = list.size();
        System.out.println("Количество товаров в избранном: " + resultCount);
        Assert.assertEquals(resultCount,1);
    return new FavoritesPage(driver);
    }


    /**
     * Метод свайпа на странице Избранного
     * https://github.com/appium/appium-uiautomator2-driver/blob/master/docs/android-mobile-gestures.md
     */
    public FavoritesPage swipeDownInFavoritesPage() {
        ((JavascriptExecutor) driver).executeScript("mobile: swipeGesture", ImmutableMap.of(
                "elementId", favoritesFeed,
                "direction", "down",
                "percent", 1.0,
                "speed", 1700
        ));
        return new FavoritesPage(driver);
    }
}
