package page;

import com.google.common.collect.ImmutableMap;
import io.appium.java_client.android.AndroidDriver;
import lombok.SneakyThrows;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


/**
 * Класс Окна выбора валют
 */
public class CurrencyView {
    /**
     * Переменная с текущим андроид драйвером
     */
    private final AndroidDriver driver;
    /**
     * Лайаут для скролла выбора валюты
     */
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView")
    private WebElement currencyLayout;
    /**
     * Радио-баттон элемента Dollar'
     */
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RadioButton[9]")
    private WebElement currencyRadioButtonDollar;

    /**
     * Метод Скролла в окне валют
     * https://github.com/appium/appium-uiautomator2-driver/blob/master/docs/android-mobile-gestures.md
     */
    public CurrencyView scrollCurrencyView() {
        ((JavascriptExecutor) driver).executeScript("mobile: scrollGesture", ImmutableMap.of(
                "elementId",currencyLayout,
                "direction", "down",
                "percent", 6.0,
                "speed",1700
        ));
        return new CurrencyView(driver);
    }
    /**
     * Метод для теста, проверяющий что нужный элемент на странице
     * Выполняет выбор валюты Доллар и переходит на страницу SettingPage
     */
    public SettingPage clickCurrencyRadioButtonDollarTransitionSettingPage() {
        try {
            WebElement button = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                    .until(ExpectedConditions.visibilityOf(currencyLayout));
            boolean isDisplayed = button.isDisplayed();
            if (isDisplayed = true) {
                scrollCurrencyView();
                currencyRadioButtonDollar.click();
                return new SettingPage(driver);
            }
        } catch (TimeoutException e) {
            System.out.println("Радиобаттон Dollar отсутствует," +
                    "- обратись в разработчику!");
        }
        return new SettingPage(driver);
    }
    /**
     * Конструктор создания страницы
     * @param androidDriver экземпляр драйвера
     */
    @SneakyThrows
    public CurrencyView(AndroidDriver androidDriver) {
        driver = androidDriver;
        PageFactory.initElements(driver, this);
    }
}