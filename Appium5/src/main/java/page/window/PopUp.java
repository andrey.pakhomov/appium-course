package page.window;

import io.appium.java_client.android.AndroidDriver;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.JoomPage;

import java.time.Duration;

/**
 * Класс страницы PopUp
 */
    public class PopUp {
    /**
     * Переменная с текущим андроид драйвером
     */
    private final AndroidDriver driver;

    /**
     * Элемент закрытия всплывающего виджета
     */
    @FindBy(id = "com.joom:id/bottom_sheet_indicator")
    private WebElement widgetClose;
    /**
     * Метод для теста, проверяющий что нужный элемент на странице
     * Выполняет закрытие PopUp окна и переходит на страницу JoomPage
     */
    public JoomPage clickStoriesCloseTransitionJoomPage() throws Exception {
        try {
            WebElement button = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                    .until(ExpectedConditions.visibilityOf(widgetClose));
            boolean isDisplayed = button.isDisplayed();
            if (isDisplayed = true) {
                clickCancelWidget();
                return new JoomPage(driver);
            }
        } catch (TimeoutException e) {
            System.out.println("PopUp отсутствует при старте приложения," +
                                "если нужно добавить - обратись в разработчику!");
        }
        return new JoomPage(driver);
    }

    /**
     * Метод закрытия всплывающего виджета на странице
     */
    public JoomPage clickCancelWidget() {
        WebElement widget = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                .until(ExpectedConditions.visibilityOf(widgetClose));
        widget.click();
        return new JoomPage(driver);
    }

    /**
     * Конструктор создания страницы
     * @param androidDriver экземпляр драйвера
     */
    public PopUp(AndroidDriver androidDriver){
        driver = androidDriver;
        PageFactory.initElements(driver, this);
    }
}