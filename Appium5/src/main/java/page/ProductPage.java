package page;

import io.appium.java_client.android.AndroidDriver;
import lombok.SneakyThrows;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Duration;

/**
 * Класс страницы Продукта (карточка товара)
 */
public class ProductPage {
    /**
     * Переменная с текущим андроид драйвером
     */
    private final AndroidDriver driver;
    /**
     * Элемент цена товара
     */
    @FindBy(id = "com.joom:id/product_info_current_price_label")
    private WebElement currencyProduct;
    /**
     * Метод получения текста цена товара
     */
    public String getPriceProductText(){
        System.out.println("Цена товара первой карточки: " + currencyProduct.getText());
        return currencyProduct.getText();
    }
    /**
     * Ожидаемый результат - наличие в цене Доллара
     */
    public String expectedResult = "$";
    /**
     * Метод для теста, проверяющий что нужный элемент на странице
     * Выполняет сравнение цены
     */
    public ProductPage assertCurrencyProductPage() {
        try {
            WebElement button = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                    .until(ExpectedConditions.visibilityOf(currencyProduct));
            boolean isDisplayed = button.isDisplayed();
            if (isDisplayed = true) {
               String result = getPriceProductText();
               Assert.assertTrue(result.contains(expectedResult),"Цена не содержит $");
               return new ProductPage(driver);
            }
        } catch (TimeoutException e) {
            System.out.println("Цена товара отсутствует," +
                    "- обратись в разработчику!");
        }
        return new ProductPage(driver);
    }
    /**
     * Элемент Like - добавление в избранное
     */
    @FindBy(id = "com.joom:id/like_button")
    private WebElement likeButton;

    /**
     * Метод нажатия на Like (Добавление в избранное)
     */
    @SneakyThrows
    public void clickLikeButton() {
        likeButton.click();
        Thread.sleep(2000);
    }
    /**
     * Элемент <- кнопка назад
     */
    @FindBy(xpath = "//android.widget.ImageView[@content-desc=\"Back\"]")
    private WebElement backButton;
    /**
     * Метод нажатия <- кнопкy назад
     */
    @SneakyThrows
    public void clickBackButton() {
        backButton.click();
        Thread.sleep(2000);
    }

    /**
     * Метод для теста, проверяющий что нужный элемент на странице
     * Выполняет нажатие на Like - Добавляет в избранное
     *Нажимает на кнопку назад <- и переходит на страницу JoomPage
     */
    public JoomPage clickLikeButtonAndClickBackButtonTransitionJoomPage() {
        try {
            WebElement button = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                    .until(ExpectedConditions.visibilityOf(backButton));
            boolean isDisplayed = button.isDisplayed();
            if (isDisplayed = true) {
                clickLikeButton();
                clickBackButton();
                return new JoomPage(driver);
            }
        } catch (TimeoutException e) {
            System.out.println("Нет элемента выход из карточки товара," +
                    "- обратись в разработчику!");
        }
        return new JoomPage(driver);
    }

    /**
     * Конструктор создания страницы
     * @param androidDriver экземпляр драйвера
     */
    @SneakyThrows
    public ProductPage(AndroidDriver androidDriver) {
        driver = androidDriver;
        PageFactory.initElements(driver, this);
    }
}
