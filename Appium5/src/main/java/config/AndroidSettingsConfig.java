package config;

import org.aeonbits.owner.Config;

/**
 * Config класс возвращающий параметры для android driver.
 */
@Config.Sources({"classpath:config/config.properties"})
public interface AndroidSettingsConfig extends Config {

    /**
     * Адрес для локального запуска
     */
    String url();

    /**
     * Имя устройства
     */
    String deviceName();

    /**
     * ОС
     */
    String platformName();

    /**
     * Версия ОС
     */
    String platformVersion();

    /**
     * Id эмулятора
     */
    String udid();

    /**
     * Адрес приложения
     */
    String app();

    /**
     *  Пакет с активити
     */
    String appWaitActivity();
    /**
     *  Пакет с ожиданием активити
     */
    String appWaitDuration();


}