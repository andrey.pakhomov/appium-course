package config;

import org.aeonbits.owner.Config;

/**
 * Config класс возвращающий параметры для android driver.
 */
@Config.Sources({"classpath:config/config.properties"})
public interface AndroidSettingsConfig extends Config {

    /**
     * Адрес для локального запуска
     */
    String url();

    /**
     * Имя устройства
     */
    String deviceName();

    /**
     * ОС
     */
    String platformName();

    /**
     * Версия ОС
     */
    String platformVersion();

    /**
     * Id эмулятора
     */
    String udid();

    /**
     * Адрес приложения
     */
    String app();

    /**
     *  Пакет с активити
     */
    String appWaitActivity();
    /**
     *  Пакет с ожиданием активити
     */
    String appWaitDuration();

    //Настройки для запуска в облаке

    /**
     * Адрес URL BrowserStack
     */
    String urlBrowserStack();
    /**
     * Адрес приложения BrowserStack
     */
    String appBrowserStack();
    /**
     * Имя устройства в BrowserStack
     */
    String deviceNameBrowserStack();
    /**
     * Версия операционной системы в BrowserStack
     */
    String versionOSBrowserStack();


}