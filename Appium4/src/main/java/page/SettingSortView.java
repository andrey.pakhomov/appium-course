package page;

import io.appium.java_client.android.AndroidDriver;
import lombok.SneakyThrows;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

/**
 * Класс страницы настроек Сортировки SettingSortView
 */
public class SettingSortView {
    /**
     * Переменная с текущим андроид драйвером
     */
    private final AndroidDriver driver;

    /**
     * Элемент радио-баттон настройки сортировки "Цена по возрастанию"
     */
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RadioButton[2]")
    private WebElement ascendingPriceRadioButton;
    /**
     * Элемент Kнопка Show настройки сортировки
     */
    @FindBy(id = "com.joom:id/button_title")
    private WebElement showButton;
    /**
     * Метод выбора цены по возрастанию
     */
    @SneakyThrows
    public void clickAscendingPriceRadioButton() {
        ascendingPriceRadioButton.click();
        Thread.sleep(3000);
    }

    /**
     * Метод для теста, проверяющий что открыта страница настроек сортировки
     * Выполняет выбор цены по возрастанию
     * Переходит на страницу SearchResultPage
     */
    @SneakyThrows
    public SearchResultPage clickAscendingPriceRadioButtonAndTransitionSearchResultPage() {
        try {
            WebElement input = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                    .until(ExpectedConditions.visibilityOf(showButton));
            boolean isDisplayed = input.isDisplayed();
            if (isDisplayed = true) {
               clickAscendingPriceRadioButton();
                return new SearchResultPage(driver);
            }
        } catch (TimeoutException e) {
            System.out.println("Окно настроек сортировки не открыто," +
                    "- обратись в разработчику!");
        }
        return new SearchResultPage(driver);
    }

    /**
     * Конструктор создания страницы
     * @param androidDriver экземпляр драйвера
     */
    public SettingSortView(AndroidDriver androidDriver) {
        driver = androidDriver;
        PageFactory.initElements(driver, this);
    }

}
