package page;

import io.appium.java_client.android.AndroidDriver;
import lombok.SneakyThrows;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

/**
 * Класс страницы Профиль
 */
public class ProfilePage {
    /**
     * Переменная с текущим андроид драйвером
     */
    private final AndroidDriver driver;
    /**
     * Элемент-кнопка настройки пользователя
     */
    @FindBy(xpath = "//android.widget.ImageView[@content-desc=\"Settings\"]")
    private WebElement settingButton;

    /**
     * Метод для теста, проверяющий что нужный элемент на странице
     * Выполняет переход на страницу настроек
     */
    public SettingPage clickSettingButtonTransitionSettingPage() throws Exception {
        try {
            WebElement button = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                    .until(ExpectedConditions.visibilityOf(settingButton));
            boolean isDisplayed = button.isDisplayed();
            if (isDisplayed = true) {
                clickSettingButton();
                return new SettingPage(driver);
            }
        } catch (TimeoutException e) {
            System.out.println("Кнопка отсутствует или изменен xpath," +
                    "- обратись в разработчику!");
        }
        return new SettingPage(driver);
    }

    /**
     * Метод нажатия на кнопку Настройки
     */
    public void clickSettingButton() {
        settingButton.click();
    }
    /**
     * Элемент кнопка назад <- на Toolbar
     */
    @FindBy(xpath = "//android.widget.ImageView[@content-desc=\"Home\"]")
    private WebElement navigationHomeButton;
    /**
     * Метод нажатия на кнопку Домой
     */
    public void clickNavigationHomeButton(){
        navigationHomeButton.click();
    }
    /**
     * Метод для теста, проверяющий что нужный элемент на странице
     * Выполняет нажатие на кнопку Назад (<-) и переходит на страницу JoomPage
     */
    public JoomPage clickNavigationHomeButtonTransitionJoomPage(){
        try {
            WebElement chapter = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                    .until(ExpectedConditions.visibilityOf(navigationHomeButton));
            boolean isDisplayed = chapter.isDisplayed();
            if (isDisplayed = true) {
                clickNavigationHomeButton();
                return new JoomPage(driver);
            }
        } catch (TimeoutException e) {
            System.out.println("Кнопка Назад <- отсутствует," +
                    "- обратись в разработчику!");
        }
        return new JoomPage(driver);
    }
    /**
     * Элемент кнопка Favorites
     */
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.Button[1]/android.widget.TextView")
    private WebElement favoritesButton;
    @SneakyThrows
    public FavoritesPage clickFavoritesButtonTransitionFavoritesPage() {
        favoritesButton.click();
        Thread.sleep(2000);
        return new FavoritesPage(driver);
    }

    /**
     * Конструктор создания страницы
     * @param androidDriver экземпляр драйвера
     */
    public ProfilePage(AndroidDriver androidDriver){
        driver = androidDriver;
        PageFactory.initElements(driver, this);
    }
}
