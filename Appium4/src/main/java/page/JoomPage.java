package page;

import com.google.common.collect.ImmutableMap;
import io.appium.java_client.android.AndroidDriver;
import lombok.SneakyThrows;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

/**
 * Класс главной страницы Joom
 */
public class JoomPage {
    /**
     * Переменная с текущим андроид драйвером
     */
    private final AndroidDriver driver;
    /**
     * Элемент-кнопка профиль пользователя
     */
    @FindBy(xpath = "//android.widget.ImageView[@content-desc=\"Profile\"]")
    private WebElement profileButton;

    /**
     * Метод для теста, проверяющий что нужный элемент на странице
     * Выполняет нажатие на кнопку Профиль пользователя и переходит на страницу ProfilePage
     */
    public ProfilePage clickProfileButtonTransitionProfilePage() {
        try {
            WebElement button = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                    .until(ExpectedConditions.visibilityOf(profileButton));
            boolean isDisplayed = button.isDisplayed();
            if (isDisplayed = true) {
                clickProfileButton();
                return new ProfilePage(driver);
            }
        } catch (TimeoutException e) {
            System.out.println("Кнопка профиль отсутствует," +
                    "- обратись в разработчику!");
        }
        return new ProfilePage(driver);
    }

    /**
     * Метод нажатия на кнопку профиля пользователя
     */
    public void clickProfileButton() {
        profileButton.click();
    }

    /**
     * Элемент первая карточка товара
     */
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.FrameLayout[3]/androidx.viewpager.widget.a/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]")
    private WebElement productButton;

    /**
     * Метод нажатия на кнопку Карточки товара
     */
    public void clickProductButton() {
        productButton.click();
    }

    /**
     * Метод для теста, проверяющий что нужный элемент на странице
     * Выполняет нажатие на первую карточку товара и переходит на страницу ProductPage
     */
    public ProductPage clickProductButtonTransitionProductPage() {
        try {
            WebElement button = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                    .until(ExpectedConditions.visibilityOf(productButton));
            boolean isDisplayed = button.isDisplayed();
            if (isDisplayed = true) {
                clickProductButton();
                return new ProductPage(driver);
            }
        } catch (TimeoutException e) {
            System.out.println("Карточка товара отсутствует," +
                    "- обратись в разработчику!");
        }
        return new ProductPage(driver);
    }

    /**
     * Элемент поле поиска
     */
    @FindBy(id = "com.joom:id/search")
    private WebElement inputSearch;

    /**
     * Метод для теста, проверяющий что нужный элемент на странице
     * Выполняет нажатие на поле поиска и переходит на страницу SearchPage
     */
    public SettingPage clickInputSearchTransitionSearchPage() {
        try {
            WebElement input = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                    .until(ExpectedConditions.visibilityOf(inputSearch));
            boolean isDisplayed = input.isDisplayed();
            if (isDisplayed = true) {
                clickInputSearch();
                return new SettingPage(driver);
            }
        } catch (TimeoutException e) {
            System.out.println("Поле поиска отсутствует," +
                    "- обратись в разработчику!");
        }
        return new SettingPage(driver);
    }
    /**
     * Элемент Кнопка раздела Best
     */
    @FindBy(xpath = "//android.widget.LinearLayout[@content-desc=\"Best\"]")
    private WebElement bestButton;
    /**
     * Элемент Кнопка раздела Sale
     */
    @FindBy(xpath = "//android.widget.LinearLayout[@content-desc=\"\uD83D\uDC30 Sale\"]")
    private WebElement saleButton;

    /**
     * Метод нажатия на поле поиска
     */
    @SneakyThrows
    public void clickInputSearch() {
        inputSearch.click();
        Thread.sleep(2000);
        inputSearch.click();
    }
    @SneakyThrows
        public JoomPage isSelectedActiveTabs() {
            try {
                WebElement input = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                        .until(ExpectedConditions.visibilityOf(bestButton));
                boolean isDisplayed = input.isDisplayed();
                if (isDisplayed = true) {
                    if(bestButton.isSelected()==true){
                        System.out.println("Нажата кнопка Best: " + bestButton.isSelected());
                    } else if (saleButton.isSelected()==true) {
                        System.out.println("Нажата кнопка Sale: " + saleButton.isSelected());
                    }
                }
            } catch (TimeoutException e) {
                System.out.println("Поле поиска отсутствует," +
                        "- обратись в разработчику!");
            }
            return new JoomPage(driver);
        }

    /**
     * Метод свайпа на главной странице
     * https://github.com/appium/appium-uiautomator2-driver/blob/master/docs/android-mobile-gestures.md
     */
    public SearchResultPage swipeLeftInJoomPage() {
        ((JavascriptExecutor) driver).executeScript("mobile: swipeGesture", ImmutableMap.of(
                "elementId", productButton,
                "direction", "left",
                "percent", 1.0,
                "speed", 1700
        ));
        return new SearchResultPage(driver);
    }

    /**
     * Конструктор создания страницы
     *
     * @param androidDriver экземпляр драйвера
     */
    public JoomPage(AndroidDriver androidDriver) {
        driver = androidDriver;
        PageFactory.initElements(driver, this);
    }
}

