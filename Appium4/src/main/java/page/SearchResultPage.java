package page;

import com.google.common.collect.ImmutableMap;
import io.appium.java_client.android.AndroidDriver;
import lombok.SneakyThrows;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

/**
 * Класс страницы результатов поиска SearchResultPage
 */
public class SearchResultPage {
    /**
     * Переменная с текущим андроид драйвером
     */
    private final AndroidDriver driver;

    /**
     * Элемент кнопка Сортировка
     */
    @FindBy(xpath = "//android.widget.LinearLayout[@content-desc=\"Sort, None\"]/android.widget.ImageView")
    private WebElement sortButton;

    /**
     * Метод для теста, проверяющий что нужный элемент на странице
     * Выполняет клик на кнопку сортировки и переходит на страницу SettingSortView
     */
    public SettingSortView clickSortButtonTransitionSettingSortView() {
        try {
            WebElement input = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                    .until(ExpectedConditions.visibilityOf(sortButton));
            boolean isDisplayed = input.isDisplayed();
            if (isDisplayed = true) {
                clickSortButton();
                return new SettingSortView(driver);
            }
        } catch (TimeoutException e) {
            System.out.println("Кнопка сортировки отсутствует," +
                    "- обратись в разработчику!");
        }
        return new SettingSortView(driver);
    }

    /**
     * Метод нажатия на кнопку сортировки
     */
    @SneakyThrows
    public void clickSortButton() {
        sortButton.click();
        Thread.sleep(2000);
    }

    /**
     * Конструктор создания страницы
     *
     * @param androidDriver экземпляр драйвера
     */
    public SearchResultPage(AndroidDriver androidDriver) {
        driver = androidDriver;
        PageFactory.initElements(driver, this);
    }

    /**
     * Элемент Карточка товара 1
     */
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView")
    private WebElement cardElement1;
    /**
     * Элемент Карточка товара 2
     */
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.widget.TextView")
    private WebElement cardElement2;
    /**
     * Элемент Карточка товара 3
     */
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[3]/android.widget.TextView")
    private WebElement cardElement3;

    /**
     * Метод подучает цены товаров с карточек и сравнивает 1 и 2 карточку(добавил скролл для отрисовки
     * следующих карточек и возможного сравнения цен
     * @return Страницу с результатами отсортированными по возрастанию цены
     */
    @SneakyThrows
    public SearchResultPage getPrice() {
        int result1 = Integer.parseInt(cardElement1.getText().replaceAll("[^0-9]", ""));
        System.out.println("Цена товара 1 : " + result1 + "Рублей");
        Thread.sleep(2000);
        int result2 = Integer.parseInt(cardElement2.getText().replaceAll("[^0-9]", ""));
        System.out.println("Цена товара 2 : " + result2 + "Рублей");
        Thread.sleep(2000);
        scrollResultElement();
        Thread.sleep(2000);
        int result3 = Integer.parseInt(cardElement3.getText().replaceAll("[^0-9]", ""));
        System.out.println("Цена товара 3 : " + result3 + "Рублей");
        if (result1 <= result2) {
            try {
                System.out.println("Сортировка цен по возрастанию");
                ;
            } catch (NullPointerException e) {
                System.out.println(e.getMessage());
                System.out.println("Ошибка! Первая карточка дороже второй");
                driver.close();
            }
        }
            return new SearchResultPage(driver);
        }

    /**
     * Элемент Карточка товара - для скрола
     */
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]")
    private WebElement cardElementScroll;

    /**
     * Метод Скролла в окне валют
     * https://github.com/appium/appium-uiautomator2-driver/blob/master/docs/android-mobile-gestures.md
     */
    public SearchResultPage scrollResultElement() {
        ((JavascriptExecutor) driver).executeScript("mobile: scrollGesture", ImmutableMap.of(
                "elementId", cardElementScroll,
                "direction", "down",
                "percent", 1.0,
                "speed", 1700
        ));
        return new SearchResultPage(driver);
    }
}
