import config.AndroidSettingsConfig;
import io.appium.java_client.android.AndroidDriver;
import lombok.Getter;
import lombok.SneakyThrows;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.net.URL;

/**
 * Базовый тестовый класс для тестов
 */
public class BaseTest {
    /**
     * Экземпляр интерфейса с настройками драйвера
     */
    private final static AndroidSettingsConfig androidConfig = ConfigFactory.create(AndroidSettingsConfig.class, System.getenv());
    /**
     * Экземпляр андроид драйвера
     */
    @Getter
    private AndroidDriver driver;

    /**
     * Метод выполняемый перед каждым тестом
     */
    @SneakyThrows
    @BeforeMethod
    public void setUp() {
// Browser stack
//        DesiredCapabilities capabilities = new DesiredCapabilities();
////        capabilities.setCapability(AndroidCapabilityType.DEVICE_NAME, androidConfig.deviceName());
////        capabilities.setCapability(AndroidCapabilityType.PLATFORM_NAME, androidConfig.platformName());
////        capabilities.setCapability(AndroidCapabilityType.PLATFORM_VERSION, androidConfig.platformVersion());
////        capabilities.setCapability(AndroidCapabilityType.UDID, androidConfig.udid());
////        capabilities.setCapability(AndroidCapabilityType.APP, androidConfig.app());
////        capabilities.setCapability(AndroidCapabilityType.APP_WAIT_ACTIVITY, androidConfig.appWaitActivity());
////        capabilities.setCapability(AndroidCapabilityType.APP_WAIT_DURATION, androidConfig.appWaitDuration());
//
        //Sauce Labs
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platformName","Android");
        caps.setCapability("appium:deviceName","Android GoogleAPI Emulator");
        caps.setCapability("appium:deviceOrientation", "portrait");
        caps.setCapability("appium:platformVersion","12.0");
        caps.setCapability("appium:automationName", "UiAutomator2");
        caps.setCapability("appium:app", "storage:filename=joom(4.2.1).apk");
        DesiredCapabilities sauceOptions = new DesiredCapabilities();
        sauceOptions.setCapability("build", "appium-build-V8QQR");
        sauceOptions.setCapability("name", "<Проверка активной кнопки>");
        caps.setCapability("sauce:options", sauceOptions);
        URL url = new URL("https://oauth-17andrei17-8e42d:e69b5126-e26f-4f03-93db-f4541670d10b@ondemand.eu-central-1.saucelabs.com:443/wd/hub");
        driver = new AndroidDriver(url, caps);
    }

        @AfterMethod
        public void tearDown() {
            driver.quit();
    }
}
