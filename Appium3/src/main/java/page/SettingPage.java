package page;

import io.appium.java_client.android.AndroidDriver;
import lombok.SneakyThrows;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

/**
 * Класс страницы Настройки
 */
public class SettingPage {
    /**
     * Переменная с текущим андроид драйвером
     */
    private final AndroidDriver driver;
    /**
     * Элемент-раздел: валюты Currency
     */
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.Button[3]/android.widget.TextView[1]")
     private WebElement currencyChapter;

    /**
     * Метод для теста, проверяющий что нужный элемент на странице
     * Выполняет нажатие на кнопку Профиль пользователя и переходит на страницу ProfilePage
     */
    public CurrencyView clickCurrencyChapterTransitionCurrencyView(){
        try {
            WebElement chapter = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                    .until(ExpectedConditions.visibilityOf(currencyChapter));
            boolean isDisplayed = chapter.isDisplayed();
            if (isDisplayed = true) {
                clickCurrencyChapter();
                return new CurrencyView(driver);
            }
        } catch (TimeoutException e) {
            System.out.println("Раздел валюты Currency отсутствует," +
                    "- обратись в разработчику!");
        }
        return new CurrencyView(driver);
    }
    /**
     * Метод нажатия раздел выбора валюты Currency
     */
    public void clickCurrencyChapter(){
        currencyChapter.click();
    }
    /**
     * Элемент кнопка назад <- на Toolbar
     */
    @FindBy(id = "com.joom:id/toolbar_navigation_button")
    private WebElement navigationBackButton;

    /**
     * Метод нажатия на кнопку Назад (<-)
     */
    public void clickNavigationBackButton(){
        navigationBackButton.click();
    }
    /**
     * Метод для теста, проверяющий что нужный элемент на странице
     * Выполняет нажатие на кнопку Назад (<-) и переходит на страницу ProfilePage
     */
    public ProfilePage clickNavigationBackButtonTransitionProfilePage(){
        try {
            WebElement chapter = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                    .until(ExpectedConditions.visibilityOf(navigationBackButton));
            boolean isDisplayed = chapter.isDisplayed();
            if (isDisplayed = true) {
                clickNavigationBackButton();
                return new ProfilePage(driver);
            }
        } catch (TimeoutException e) {
            System.out.println("Кнопка Назад <- отсутствует " +
                    "- обратись в разработчику!");
        }
        return new ProfilePage(driver);
    }

    /**
     * Конструктор создания страницы
     * @param androidDriver экземпляр драйвера
     */
    @SneakyThrows
    public SettingPage(AndroidDriver androidDriver) {
        driver = androidDriver;
        PageFactory.initElements(driver, this);
        //Почему - то тут без паузы не срабатывает
        Thread.sleep(1000);
    }
}
