package page;
import io.appium.java_client.android.AndroidDriver;
import lombok.SneakyThrows;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

/**
 * Класс страницы поиска SearchPage
 */
public class SearchPage {
    /**
     * Переменная с текущим андроид драйвером
     */
    private final AndroidDriver driver;
    /**
     * Элемент поле ввода поиска
     */
    @FindBy(id = "com.joom:id/input")
    private WebElement inputSearch;

    /**
     * Элемент выбор из предложенного
     */
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.Button[3]/android.widget.TextView")
    private WebElement textButton;
    /**
     * Метод для теста, проверяющий что нужный элемент на странице
     * Выполняет ввод текста "Платья" в поле ввода
     * Нажатие на предложенный вариант для поиска и переходит на страницу SearchResultPage
     */
    public SearchResultPage clickAndSendKeysForInputSearchTransitionSearchResultPage() {
        try {
            WebElement input = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                    .until(ExpectedConditions.visibilityOf(inputSearch));
            boolean isDisplayed = input.isDisplayed();
            if (isDisplayed = true) {
                clickAndSendKeysForInputSearch();
                clickTextButton();
                return new SearchResultPage(driver);
            }
        } catch (TimeoutException e) {
            System.out.println("Поле ввода для поиска отсутствует," +
                    "- обратись в разработчику!");
        }
        return new SearchResultPage(driver);
    }

    /**
     * Метод ввода в поле поиска
     */
    @SneakyThrows
    public void clickAndSendKeysForInputSearch() {
        inputSearch.click();
        inputSearch.sendKeys("Платья");
        Thread.sleep(2000);
    }
    /**
     * Метод клика на предложенный вариант
     */
    public void clickTextButton() {
        textButton.click();
    }
    /**
     * Конструктор создания страницы
     * @param androidDriver экземпляр драйвера
     */
    public SearchPage(AndroidDriver androidDriver) {
        driver = androidDriver;
        PageFactory.initElements(driver, this);
    }

}
