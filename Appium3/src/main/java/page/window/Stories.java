package page.window;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.JoomPage;

import java.time.Duration;
    /**
    * Класс страницы Stories
    */
    public class Stories {
            /**
             * Переменная с текущим андроид драйвером
             */
            private final AndroidDriver driver;

            /**
             * Элемент закрытия всплывающих Stories
             */
            @FindBy(id = "com.joom:id/close_button")
            private WebElement storiesClose;
        /**
         * Метод для теста, проверяющий что нужный элемент на странице
         * Выполняет закрытие Stories и выполняет переход на страницу JoomPage
         */
            public JoomPage clickStoriesCloseTransitionJoomPage() throws Exception {
                try {
                    WebElement button = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                            .until(ExpectedConditions.visibilityOf(storiesClose));
                    boolean isDisplayed = button.isDisplayed();
                    if (isDisplayed = true) {
                        clickStoriesCancel();
                        return new JoomPage(driver);
                    }
                } catch (TimeoutException e) {
                    System.out.println("Stories отсутствует при старте приложения," +
                            "если нужно добавить - обратись в разработчику!");
                }
                return new JoomPage(driver);
            }

            /**
             * Метод закрытия всплывающего Stories
             */
            public JoomPage clickStoriesCancel() {
                WebElement button = (new WebDriverWait(driver, Duration.ofSeconds(5L)))
                        .until(ExpectedConditions.visibilityOf(storiesClose));
                button.click();
                return new JoomPage(driver);
            }

            /**
             * Конструктор создания страницы
             * @param androidDriver экземпляр драйвера
             */
            public Stories(AndroidDriver androidDriver){
                driver = androidDriver;
                PageFactory.initElements(driver, this);
            }
    }
